import React, {Component} from 'react';
import {Map, Marker, GoogleApiWrapper, InfoWindow} from 'google-maps-react';
import * as customer from './../utlis/customer.png'
import './_help.scss';

export class Container extends Component {

    items = [
        {name: 'element-0', nameSurname: 'Andrzej', cords: {lat: 52.221375, lng: 21.042230}, activity: 'Wyprowadzanie psa'},
        {name: 'element-1', nameSurname: 'Andrzej', cords: {lat: 52.223750, lng: 21.042230}, activity: 'Wyrzucenie śmieci'},
        {name: 'element-2', nameSurname: 'Andrzej2', cords: {lat: 52.223050, lng: 21.042530}, activity: 'Umycie naczyń'},
        {name: 'element-3', nameSurname: 'Andrzej3', cords: {lat: 52.221050, lng: 21.042530}, activity: 'Posprzątanie mieszkania'},
        {name: 'element-4', nameSurname: 'Andrzej4', cords: {lat: 52.221050, lng: 21.041530}, activity: 'Umycie samochodu'},
        {name: 'element-5', nameSurname: 'Andrzej5', cords: {lat: 52.221050, lng: 21.041530}, activity: 'Umycie okien'},
        {name: 'element-6', nameSurname: 'Andrzej6', cords: {lat: 52.221050, lng: 21.035530}, activity: 'Skoszenie trawnika'},
        {name: 'element-7', nameSurname: 'Andrzej7', cords: {lat: 52.226050, lng: 21.035530}, activity: 'Skoszenie trawnika'},
        {name: 'element-8', nameSurname: 'Andrzej8', cords: {lat: 52.226050, lng: 21.031530}, activity: 'Nareperowanie płotu'},
        {name: 'element-9', nameSurname: 'Andrzej9', cords: {lat: 52.226050, lng: 20.931530}, activity: 'Pomalowanie płotu'},
        {name: 'element-10', nameSurname: 'Andrzej0', cords: {lat: 52.226050, lng: 20.85530}, activity: 'Cięcie drewna na opał'},
        {name: 'element-11', nameSurname: 'Piotrek', cords: {lat: 52.226050, lng: 20.85530}, activity: 'Pomoc przy sadzeniu pomidorów'},
        {name: 'element-12', nameSurname: 'Piotrek2', cords: {lat: 52.226050, lng: 20.97530}, activity: 'Posprzątanie mieszkania'},
        {name: 'element-13', nameSurname: 'Piotrek3', cords: {lat: 52.226050, lng: 20.99330}, activity: 'Zdjęcie mebli ze ściany'},
        {name: 'element-14', nameSurname: 'Piotrek4', cords: {lat: 52.226050, lng: 21.00000}, activity: 'Usunięcie usterki w dzwiach'},
        {name: 'element-15', nameSurname: 'Piotrek5', cords: {lat: 52.224050, lng: 21.05000}, activity: 'Wyprowadzanie psa'},
        {name: 'element-16', nameSurname: 'Piotrek6', cords: {lat: 52.228050, lng: 21.00600}, activity: 'Wyprowadzaniae psa'},
        {name: 'element-17', nameSurname: 'Piotrek7', cords: {lat: 52.221050, lng: 21.00600}, activity: 'Pomalowanie płotu'},
        {name: 'element-18', nameSurname: 'Piotrek8', cords: {lat: 52.221950, lng: 21.00400}, activity: 'Skoszenie trawy'},
        {name: 'element-19', nameSurname: 'Piotrek9', cords: {lat: 52.221950, lng: 21.01700}, activity: 'Pomoc w wyniesieniu mebli'},
        {name: 'element-20', nameSurname: 'Piotrek10', cords: {lat: 52.221950, lng: 21.01000}, activity: 'Wyprowadzanie psa'}];

    state = {
        showingInfoWindow: false,
        activeMarker: {},
        activeMarkerName: {},
        selectedPlace: {},
    };

    onMarkerClick = (props, marker, e, markerName) => {
        console.log(e);
        this.setState({
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true,
            activeMarkerName: markerName
        });
    }

    onMapClicked = (props) => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null,
                activeMarkerName: ''
            })
        }
    };

    render() {
        return (
            <div className="help">
                <h1>MAPA ZLECEŃ</h1>
                <div className="help__map">
                    <Map google={this.props.google}
                         onClick={this.onMapClicked}
                         style={{width: 'calc(100% - 20px)', height: '100%', position: 'relative'}}
                         initialCenter={{
                             lat: 52.229675,
                             lng: 21.012230
                         }}
                         className={'help__map-google'}
                         zoom={14}>
                        {
                            this.items.map((data, index) => <Marker
                                key={index}
                                title={'The marker`s title will .'}
                                name={'Dolores park'}
                                onClick={(props, marker, e) => this.onMarkerClick(props, marker, e, data.name)}
                                position={data.cords}>
                            </Marker>)
                        }
                        {
                            this.items.map((data, index) => <InfoWindow
                                key={index}
                                marker={this.state.activeMarker}
                                visible={this.state.showingInfoWindow && this.state.activeMarkerName === data.name}>
                                <form action="" className="info-window">
                                    <div className="info-window__avatar-mobile">
                                        <img src={customer} alt=""/>
                                    </div>
                                    <div className="info-window-left">
                                        <input className="info-window-left__input" type="text" value={`${data.nameSurname}`}
                                               style={{width: '100%'}} readOnly/>
                                        <input className="info-window-left__input" type="text" value={"10/12/2019"}
                                               style={{width: '60%'}} readOnly/>
                                        <input className="info-window-left__input" type="text" value={"12:00"}
                                               style={{width: '35%'}} readOnly/>
                                        <input className="info-window-left__input" type="text" value={"45m"}
                                               style={{width: '40%'}} readOnly/>
                                        <input className="info-window-left__input" type="text" value={"20zł"}
                                               style={{width: '65%'}} readOnly/>
                                        <input className="info-window-left__input" type="text"
                                               value={data.activity} style={{width: '90%'}} readOnly/>
                                    </div>
                                    <div className="info-window-right">
                                        <div className="info-window-right__avatar">
                                            <img src={customer} alt=""/>
                                        </div>
                                        <button className="info-window-right__button">Przyjmij</button>
                                    </div>
                                </form>
                            </InfoWindow>)
                        }
                    </Map>
                </div>
            </div>
        )
    }
}

export const Help = GoogleApiWrapper({
    apiKey: 'AIzaSyAyesbQMyKVVbBgKVi2g6VX7mop2z96jBo'
})(Container);