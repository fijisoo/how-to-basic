import React, {Component} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import clsx from 'clsx';
import IconButton from '@material-ui/core/IconButton';
import green from '@material-ui/core/colors/green';
import CloseIcon from '@material-ui/icons/Close';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import './_work.scss';

export class Work extends Component {

    state = {
        open: false,
    };

    setOpen = (isOpen) => this.setState((state) => state.open = isOpen);

    handleClick = () => this.setOpen(true);

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setOpen(false);
    };

    render() {
        return <div className="work">
            <div className="work__header">
                <h1>WYPEŁNIJ WSZYSTKIE ELEMENTY</h1>
            </div>
            <form action="#" className="work__form">
                <label className="work__form-element work__form-element-label" htmlFor=""><input type="text"
                                                                                                 placeholder="Lokalizacja zleceń"
                                                                                                 style={{width: '250px'}}/></label>
                <div className="work__form-element">
                    <input type="text" placeholder="Data" style={{width: '100px'}}/>
                    <input type="text" placeholder="Godzina" style={{width: '80px'}}/>
                </div>
                <input className="work__form-element" type="text" placeholder="Czas pracy" style={{width: '80px'}}/>
                <input className="work__form-element" type="text" placeholder="Wynagrodzenie" style={{width: '120px'}}/>
                <input className="work__form-element" type="text" placeholder="Kategoria usługi"
                       style={{width: '160px'}}/>
                <textarea className="work__form-element" name="opis" cols="30" rows="10"
                          placeholder="Dokładny opis pracy"/>
                <button type="button" className="work__form-submit" onClick={this.handleClick}>ZATWIERDŹ</button>
            </form>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={this.state.open}
                autoHideDuration={6000}
                onClose={this.handleClose}
            >
                <MySnackbarContentWrapper
                    onClose={this.handleClose}
                    variant="success"
                    message="This is a success message!"
                />
            </Snackbar>
        </div>
    }
}

const variantIcon = {
    success: CheckCircleIcon
};

const useStyles1 = makeStyles(theme => ({
    success: {
        backgroundColor: green[600],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
}));

function MySnackbarContentWrapper(props) {
    const classes = useStyles1();
    const {className, message, onClose, variant, ...other} = props;
    const Icon = variantIcon[variant];

    return (
        <SnackbarContent
            className={clsx(classes[variant], className)}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
                <Icon className={clsx(classes.icon, classes.iconVariant)}/>
                    {message}
        </span>
            }
            action={[
                <IconButton key="close" aria-label="Close" color="inherit" onClick={onClose}>
                    <CloseIcon className={classes.icon}/>
                </IconButton>,
            ]}
            {...other}
        />
    );
}