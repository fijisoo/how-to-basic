import React from 'react';
import {Link} from "react-router-dom";
import backgroundImg from './../utlis/main.jpg';
import './_main.scss';

export function Main() {
    return <div className="main">
        <h1>SZUKAM</h1>
        <div className="main__links">
            <li className="main__links-single">
                <Link className="main__links-single-anchor main__links-single-anchor--blue" to="/help/">PRACY</Link>
            </li>
            <li className="main__links-single">
                <Link className="main__links-single-anchor main__links-single-anchor--red" to="/work/">POMOCY</Link>
            </li>
        </div>
        <img src={backgroundImg} className="main__background-img" alt="background"/>
        <div className="main--left"/>
        <div className="main--right"/>
    </div>
}