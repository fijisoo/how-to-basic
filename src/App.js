import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import {Main} from "./pages/Main";
import {Help} from "./pages/Help";
import {Work} from "./pages/Work";

function App() {
    return (
        <Router style={{height: '100vh'}}>
            <div style={{height: '100vh'}}>
                <Route path="/" exact component={Main} />
                <Route path="/help/" component={Help} />
                <Route path="/work/" component={Work} />
            </div>
        </Router>
    );
}

export default App;